const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

function addBookList() {
    const wrapper = document.getElementById("root");
    const bookList = document.createElement("ul");
    books.forEach((book) => {
        try{
            if(!book.author) {
                throw console.log(`Параметр autor отсутсвует в книге: ${book.name}`);
            } else if (!book.name) {
                throw console.log(`Параметр name отсутсвует у автора: ${book.author}`);
            } else if (!book.price) {
                throw console.log(`Параметр price отсутствует в книге: ${book.name}`);
            };
            const liList = document.createElement("li");
            liList.textContent = `Название книги: ${book.name}, Автор книги: ${book.author}, Цена книги: ${book.price}`;
            bookList.append(liList);
            wrapper.append(bookList);
        }catch(error){
            // console.error(error);
        }
        
    })
}
addBookList(books);